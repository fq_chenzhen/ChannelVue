
const app = {
  state: {
    enum: {
      maturity: [
        {
          label: '0%',
          value: '0%'
        },
        {
          label: '10%',
          value: '10%'
        },
        {
          label: '20%',
          value: '20%'
        },
        {
          label: '30%',
          value: '30%'
        },
        {
          label: '50%',
          value: '50%'
        },
        {
          label: '80%',
          value: '80%'
        },
        {
          label: '90%',
          value: '90%'
        },
        {
          label: '已签',
          value: '已签'
        }
      ],
      contractType: [
        {
          label: '服务内',
          value: '服务内'
        },
        {
          label: '建设中',
          value: '建设中'
        },
        {
          label: '维保期内',
          value: '维保期内'
        },
        {
          label: '新客户拓展',
          value: '新客户拓展'
        },
        {
          label: '续保',
          value: '续保'
        },
        {
          label: '老客户新增',
          value: '老客户新增'
        }
      ],
      direction: [
        {
          label: '系统集成',
          value: '系统集成'
        },
        {
          label: '软件开发',
          value: '软件开发'
        },
        {
          label: '商品销售',
          value: '商品销售'
        },
        {
          label: '运行维护',
          value: '运行维护'
        },
        {
          label: '小产品',
          value: '小产品'
        }
      ],
      projectLevel: [
        {
          label: '一般跟进',
          value: '一般跟进'
        },
        {
          label: '中等跟进',
          value: '中等跟进'
        },
        {
          label: '重点跟进',
          value: '重点跟进'
        }
      ],
      territory: [
        {
          value: null,
          label: null
        },
        {
          label: '服务一组',
          value: '服务一组'
        },
        {
          label: '服务二组',
          value: '服务二组'
        },
        {
          label: '服务三组',
          value: '服务三组'
        },
        {
          label: '服务四组',
          value: '服务四组'
        },
        {
          label: '服务五组',
          value: '服务五组'
        },
        {
          label: '服务六组',
          value: '服务六组'
        },
        {
          label: '服务七组',
          value: '服务七组'
        },
        {
          label: '运营组',
          value: '运营组'
        },
        {
          label: '海南分公司',
          value: '海南分公司'
        }
      ],
      advancingState: [
        {
          label: '是',
          value: '是'
        },
        {
          label: '否',
          value: '否'
        },
        {
          label: '搁置',
          value: '搁置'
        },
        {
          label: '失败',
          value: '失败'
        }
      ],
      signType: [
        {
          label: '自签',
          value: '自签'
        },
        {
          label: '协签',
          value: '协签'
        }
      ],
      channelType:[
        {
          value: 1,
          label: '业主单位'
        },
        {
          value: 2,
          label: '行业设计院'
        },
        {
          value: 4,
          label: '全行业综合型单位'
        },
        {
          value: 8,
          label: '垂直行业的集成商'
        },
        {
          value: 16,
          label: '目标客户关系渠道'
        },
        {
          value: 32,
          label: '全国性直接竞争对手'
        },
        {
          value: 64,
          label: '区域性竞争对手'
        },
        {
          value: 128,
          label: '潜在行业竞争对手'
        },
        {
          value: 256,
          label: '下游供应商'
        },
        {
          value: 512,
          label: '其他'
        }
      ],
      channelImportance: [
        {
          value: 1,
          label: '一星潜力'
        },
        {
          value: 2,
          label: '二星潜力'
        },
        {
          value: 3,
          label: '三星潜力'
        },
        {
          value: 4,
          label: '四星潜力'
        },
        {
          value: 5,
          label: '五星潜力'
        }
      ],
      channelRate: [
        {
          value: 0,
          label: 'P0：未有沟通',
          description: '产生邮件、微信、电话互动，即对方有应答'
        },
        {
          value: 1,
          label: 'P1：远程互动状态',
          description: '有见面拜访'
        },
        {
          value: 2,
          label: 'P2：拜访互动状态',
          description: '有见面拜访'
        },
        {
          value: 3,
          label: 'P3：规模性交流',
          description: ''
        },
        {
          value: 4,
          label: 'P4：具体合作状态',
          description: '沟通或配合操作具体合作项目'
        },
        {
          value: 5,
          label: 'P5：合作成功状态',
          description: '产生某个项目或方向成功合作记录'
        },
        {
          value: 6,
          label: 'P6：合作一次以上',
          description: ''
        }
      ],
      channelFollowingType:[
        {
          value: 2,
          label: '远程沟通记录'
        },
        {
          value: 3,
          label: '拜访记录'
        }
      ],
      channelSourceType:[
        {
          value: '拓展拜访认识',
          label: '拓展拜访认识'
        },
        {
          value: '营销展会认识',
          label: '营销展会认识'
        },
        {
          value: '公司外部介绍认识',
          label: '公司外部介绍认识'
        },
        {
          value: '公司内部人员介绍',
          label: '公司内部人员介绍'
        },
        {
          value: '主动联系我们',
          label: '主动联系我们'
        },
        {
          value: '其他',
          label: '其他'
        },
      ],
      channelFollowRate:[
        {
          value: '每月一次',
          label: '每月一次'
        },
        {
          value: '每月两次',
          label: '每月两次'
        },
        {
          value: '每季一次',
          label: '每季一次'
        },
        {
          value: '节日问候',
          label: '节日问候'
        },
        {
          value: '无需联系',
          label: '无需联系'
        }
      ]
    }
  },
  mutations: {
    TOGGLE_ENUM: (state, data) => {
      // Cookies.set('enum', JSON.stringify(data))
      state.enum = data
    }
  },
  actions: {
    ToggleEnum({ commit }, data) {
      commit('TOGGLE_ENUM', data)
    }
  }
}

export default app
