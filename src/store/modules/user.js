import Cookies from 'js-cookie'
import { getAllUsers } from '@/api/login'

const user = {
  state: {
      userInfo: {
      id: '',
      name: '',
      department: '',
      isEdit: false
    },
    users:[]
  },
  mutations: {
    SET_USERINFO: (state, userInfo) => {
      state.userInfo = userInfo
    },
    GET_USERS:(state, data) =>{
      state.users = data
    }
  },
  actions: {
    // 登出
    logout() {
      return new Promise((resolve) => {
        Cookies.remove('isLogin')
        Cookies.remove('userInfo')
        Cookies.remove('full_name')
        resolve()
      })
    },
    getUsers({ commit }, data){
      return new Promise(resolve =>{
          getAllUsers().then(res => {
                  if (res.data.success) {
                    var result =  res.data.data.map(d=>{
                        d.value = d.userName
                        d.label = d.userName
                        return d
                      })
                    commit('GET_USERS', result)
                    resolve(result)
                  }
          })
      })
    }
  }
}

export default user
