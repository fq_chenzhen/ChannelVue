const getters = {
  userInfo: state => state.user.userInfo,
  appEnum: state => state.app.enum,
  users: state => state.user.users,
  permission_routers: state => state.permission.routers,

  channelRateTexts: state => {
    return state.app.enum.channelRate.map(x => {
                  return `${x.label}：${x.description}`
          })
  }
}
export default getters
