import request from '@/utils/request'

// 获取站点最后来报信息列表
const getLastTm = (query) => {
  return request({
    url: '/api/site/lasttm',
    method: 'get',
    params: query
  })
}

// 获取站点统计信息
const getStatis = (query) => {
  return request({
    url: '/api/site/statisunit',
    method: 'get',
    params: query
  })
}

// 获取站点统计信息(省份)
const getStatisArea = (query) => {
  return request({
    url: '/api/site/statisarea',
    method: 'get',
    params: query
  })
}

// 获取站点统计信息（责任区域）
const getStatisTe = (query) => {
  return request({
    url: '/api/site/statisterritory',
    method: 'get',
    params: query
  })
}

// 更新测站信息
const updateStbprpb = (data) => {
  return request({
    url: '/api/site/stbprpb',
    method: 'post',
    data
  })
}

// 获取上报单位信息
const getReported = (query) => {
  return request({
    url: '/api/site/reported',
    method: 'get',
    params: query
  })
}

// 新增修改上报单位
const addReported = (data) => {
  return request({
    url: '/api/site/reported',
    method: 'post',
    data
  })
}

// 删除上报单位
const delReported = (query) => {
  return request({
    url: '/api/site/reported',
    method: 'delete',
    params: query
  })
}

// 导出站点信息列表
const exportLastTm = (query) => {
  return request({
    url: '/api/site/exprot',
    method: 'get',
    params: query
  })
}

// 导出站点统计信息
const exportStatis = (query) => {
  return request({
    url: '/api/site/exprotstatis',
    method: 'get',
    params: query
  })
}

export {
  getLastTm,
  getStatis,
  getStatisTe,
  getStatisArea,
  updateStbprpb,
  getReported,
  addReported,
  delReported,
  exportLastTm,
  exportStatis
}
