import request from '@/utils/request'

// 查询服务合同项目信息列表
const getLists = (query) => {
  return request({
    url: '/api/channel',
    method: 'get',
    params: query
  })
}
// 导出计划表
const exprotChannel = (query) => {
  return request({
    url: '/api/channel/exprotchannel',
    method: 'get',
    params: query
  })
}

const addChannel = (data) => {
  return request({
    url: '/api/channel',
    method: 'post',
    data
  })
}

const updateChannel = (data) => {
  return request({
    url: '/api/channel',
    method: 'put',
    data
  })
}

const updateChannelBase = (data) => {
  return request({
    url: '/api/channel/updateBaseinfo',
    method: 'put',
    data
  })
}

const updateChannelValue = (data) => {
  return request({
    url: '/api/channel/updateValueinfo',
    method: 'put',
    data
  })
}

const getChannelimportanceById = (query) => {
  return request({
    url: '/api/channelimportance',
    method: 'get',
    params: query
  })
}

const getChannelfollowingAll = (query) => {
  return request({
    url: '/api/channelfollowing/all',
    method: 'get',
    params: query
  })
}

const getChannelfollowingPaging = (query) => {
  return request({
    url: '/api/channelfollowing',
    method: 'get',
    params: query
  })
}

const getChannelfollowingById = (query) => {
  return request({
    url: '/api/channelfollowing/getbyid',
    method: 'get',
    params: query
  })
}

const addChannelfollowing = (data) => {
  return request({
    url: '/api/channelfollowing',
    method: 'post',
    data
  })
}

const deleteChannelfollowing = (data) => {
  return request({
    url: '/api/channelfollowing',
    method: 'delete',
    params: data
  })
}

export {
  getLists
  ,exprotChannel
  ,addChannel
  ,updateChannel
  ,getChannelfollowingAll
  ,getChannelfollowingPaging
  ,getChannelfollowingById
  ,addChannelfollowing
  ,deleteChannelfollowing
  ,updateChannelBase
  ,updateChannelValue
  ,getChannelimportanceById
}
