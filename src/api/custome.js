import request from '@/utils/request'

// 获取计划项目列表
const getCustomeInfo = (query) => {
  return request({
    url: 'api/customer/oldinfo',
    method: 'get',
    params: query
  })
}

export {
  getCustomeInfo
}
