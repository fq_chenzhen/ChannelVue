import request from '@/utils/request'

// 用户登录
const login = (data) => {
  return request({
    url: '/api/user/login',
    method: 'post',
    data
  })
}

// 新增用户信息
const addUser = (data) => {
  return request({
    url: '/api/user',
    method: 'post',
    data
  })
}

const getAllUsers = query =>{
  return request({
    url: '/api/user/alluserinfo',
    method: 'get',
    params: query
  })
}

export {
  login,
  addUser,
  getAllUsers
}
