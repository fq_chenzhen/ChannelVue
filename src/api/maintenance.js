import request from '@/utils/request'

// 查询服务合同项目信息列表
const getMLists = (query) => {
  return request({
    url: '/api/maintenance/lists',
    method: 'get',
    params: query
  })
}

// 根据服务合同编号获取项目信息
const getMInfoById = (query) => {
  return request({
    url: '/api/maintenance',
    method: 'get',
    params: query
  })
}

// 新增/修改服务合同信息   header: 'Access-Control-Allow-Origin:*',
const addMInfo = (data) => {
  return request({
    url: '/api/maintenance',
    method: 'post',
    data
  })
}

// 验证服务合同编号是否存在
const voidMById = (query) => {
  return request({
    url: '/api/maintenance/void',
    method: 'get',
    params: query
  })
}

// 获取服务合同跟踪记录
const getMRecordsById = (query) => {
  return request({
    url: '/api/maintenance/records',
    method: 'get',
    params: query
  })
}
// 新增服务合同跟踪记录
const addMRecord = (data) => {
  return request({
    url: '/api/maintenance/record',
    method: 'post',
    data
  })
}
// 删除服务合同跟踪记录
const delMRecordById = (query) => {
  return request({
    url: '/api/maintenance/records',
    method: 'delete',
    params: query
  })
}

// 新增月份计划
const addPlanProcess = (data) => {
  return request({
    url: '/api/maintenance/planprocess',
    method: 'post',
    data
  })
}
// 获取月份计划信息
const getPlanProcess = (query) => {
  return request({
    url: '/api/maintenance/planprocess',
    method: 'get',
    params: query
  })
}

// 验证月份计划是否存在
const voidPlanProcess = (query) => {
  return request({
    url: '/api/maintenance/voidplanprocess',
    method: 'get',
    params: query
  })
}

// 删除根据ID服务合同信息
const delMInfoContractId = (query) => {
  return request({
    url: '/api/maintenance',
    method: 'delete',
    params: query
  })
}

// 导出服务合同信息
const exprotMInfo = (query) => {
  return request({
    url: '/api/maintenance/exprot',
    method: 'get',
    params: query
  })
}
// 获取逐月情况
const getDistribution = (query) => {
  return request({
    url: '/api/maintenance/distribution',
    method: 'get',
    params: query
  })
}

// 获取成熟度统计信息
const getMaturityStats = (query) => {
  return request({
    url: '/api/maintenance/maturitystats',
    method: 'get',
    params: query
  })
}

// 获取分组统计信息
const getTerritoryStats = (query) => {
  return request({
    url: '/api/maintenance/territorystats',
    method: 'get',
    params: query
  })
}

// 获取逐月完成数据
const getMonthByMonthStats = (query) => {
  return request({
    url: '/api/maintenance/statsbymonth2',
    method: 'get',
    params: query
  })
}
// 获取签订类型计划表数据
const getTypePlan = (query) => {
  return request({
    url: 'api/maintenance/plan',
    method: 'get',
    params: query
  })
}
// 获取区域计划表数据
const getAreaPlan = (query) => {
  return request({
    url: 'api/maintenance/plan2',
    method: 'get',
    params: query
  })
}
// 获取计划项目列表
const getPlanInfo = (query) => {
  return request({
    url: 'api/maintenance/planlists',
    method: 'get',
    params: query
  })
}

// 导出计划表
const exprotPlan = (query) => {
  return request({
    url: 'api/maintenance/exprotplan',
    method: 'get',
    params: query
  })
}

export {
  getMLists,
  getMInfoById,
  addMInfo,
  voidMById,
  getMRecordsById,
  addMRecord,
  delMRecordById,
  addPlanProcess,
  voidPlanProcess,
  getPlanProcess,
  delMInfoContractId,
  exprotMInfo,
  getDistribution,
  getMaturityStats,
  getTerritoryStats,
  getMonthByMonthStats,
  getTypePlan,
  getAreaPlan,
  getPlanInfo,
  exprotPlan
}
