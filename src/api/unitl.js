import request from '@/utils/request'

// 获取城市列表
const getAreaTree = (query) => {
  return request({
    url: '/api/unit/areatree',
    method: 'get',
    params: query
  })
}
export {
  getAreaTree
}
