const computeState  = (states, data) => {
  var values = []
  var labels = []

  states.map(x => {
    if ((x.value & data) == x.value) {
      labels.push(x.label)
      values.push(x.value)
    }
  })

  return {
    values,
    labels:labels.join('，')
  }
}

const sumState  = (data) => {
  var result = null
  if(!Array.isArray(data)) return result

  data.map(x=>{
    result |= x
  })
  return result
}

export {
  computeState,
  sumState
}
