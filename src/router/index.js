import Vue from 'vue'
import Router from 'vue-router'
import Cookies from 'js-cookie'
/* Layout */
import Layout from '@/views/layout/layout'
import { Message } from 'element-ui'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/',
    component: Layout,
    redirect: '/chanelinfo',
    children: [
      {
        path: 'chanelinfo',
        component: () => import('@/views/chanel/chanelinfo'),
        name: 'chanelinfo'
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/login.1'),
    hidden: true
  }
]

// export default new Router({
//   // mode: 'history', // require service support
//   scrollBehavior: () => ({ y: 0 }),
//   routes: constantRouterMap
// })
const router = new Router({
  // mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

// 白名单
const whiteList = ['/login']

router.beforeEach((to, from, next) => {
  if (Cookies.get('userInfo') || Cookies.get('isLogin')) {

    if(!JSON.parse(Cookies.get('userInfo')).isEdit){
      if (to.path === '/login') {
        next()
      } else {
        Message({
          message: '没有权限!',
          type: 'error',
          duration: 5 * 1000
        })
        next({
          path: '/login'
        })
      }
      return
    }

    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) { // 是否在白名单中
      next()
    } else {
      next({
        path: '/login'
      })
    }
  }
})
export default router
