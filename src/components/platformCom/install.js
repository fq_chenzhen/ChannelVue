// 组件全局注册
/* jshint esversion: 6 */
import Vue from 'vue'

import pageSearch from './pageSearch'
import pageTable from './pageTable'
// 形成组件库
const components = [
    pageSearch,
    pageTable
]

// 注册全局组件
components.map((com) => {
    Vue.use(com)
})

export default components
