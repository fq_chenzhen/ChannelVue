# vue-results-systems

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Vue Element-UI 问题汇总

* 组件传参可通过 this.$refs.channeldetail.channelInfoForm = this.setting.row;
* select组件的参数 用上面方式传参会有问题 参数要继承vm的原型
* table组件的展开里面异步加载时 hack，要更新下其他字段，否则然不能更新。如：row.customname = row.customname + ' '
* 原生事件 .native
