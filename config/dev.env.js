'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

// BASE_API: '"http://202.109.200.36:19001/Results/"' '"http://localhost:63672/"'
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  BASE_API: '"http://localhost:63672"'
})

